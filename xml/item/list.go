package item

import (
	"encoding/xml"

	"gitlab.com/nawia/itemeditor/model/client"
	"gitlab.com/nawia/itemeditor/model/item"
)

type xmlAttribute struct {
	//type[value=container|key|magicfield|depot|mailbox|trashholder|teleport|door|bed|rune]
	//name[value]
	//article[value]
	//plural[value]
	//bool clientid[value]
	//bool cache[value]
	//int wareid[value]
	//bool blocksolid|blocking[bool]
	//blockprojectile[bool]
	//blockpathfind|blockpathing|blockpath[bool]
	//lightlevel[int]
	//lightcolor[int]
	//description[string]
	//runespellname[string]
	//weight[int/100]
	//showcount[bool]
	//armor[int]
	//defense[int]
	//extradefense[int]
	//attack[int]
	//extraattack|extraatk[int]
	//attackspeed[int]
	//rotateto[int]
	//movable|moveable[bool]
	//verticalisvertical[int]
	//horizontal|ishorizontal[int]
	//pickupable[bool]
	//allowpickupable[bool]
	//floorchange[down|north|south|west|east|northex|southex|westex|eastex]
	//corpsetype[venom|blood|undead|fire|energy]
	//containersize[int]
	//fluidsource[string]
	//writeable|writable[bool]
	//readable[bool]
	//maxtextlen|maxtextlength[int]
	//text[string]
	//author|writer[string]
	//date[int]
	//writeonceitemid[int]
	//wareid[int]
	//worth[int]
	//forceserialize|forceserialization|forcesave[bool]
	//leveldoor[int]
	//specialdoor[bool]
	//closingdoor[bool]
	//weapontype[sword|club|axe|shield|distance|dist|wand|rod|ammunition|ammo|fist]
	//slottype[head|body|legs|feet|backpack|two-handed|right-hand|left-hand|necklace|ring|ammo|hand]
	//ammotype[string]
	//shoottype[string]
	//effect[string]
	//range[int]
	//stopduration[bool]
	//decayto[int]
	//transformequipto|onequipto[int]
	//transformdeequipto|ondeequipto[int]
	//duration[uint]
	//showduration[int]
	//charges[int]
	//showcharges[bool]
	//showattributes[bool]
	//breakchance[0..100]
	//ammoaction[string]
	Key   string `xml:"key,attr"`
	Value string `xml:"value,attr"`
}

type xmlItem struct {
	XMLName   xml.Name       `xml:"item"`
	Id        string         `xml:"id,attr"` //TODO: might include multiple values in form of "id1;id2" or ranges in form of "minId-maxId"
	Name      string         `xml:"name,attr"`
	FromId    string         `xml:"fromid,attr,omitempty"` //TODO: might include multiple values in form of "id1;id2"
	ToId      string         `xml:"toid,attr,omitempty"`   //TODO: might include multiple values in form of "id1:id2"
	Article   string         `xml:"article,attr,omitempty"`
	Plural    string         `xml:"plural,attr,omitempty"`
	Attribute []xmlAttribute `xml:"attribute"`
	//TODO: override attribute
}

type decoder struct {
	XMLName xml.Name  `xml:"items"`
	Items   []xmlItem `xml:"item"`
}

func Decoder(items *item.List, itemVersions *[]item.Version, clientVersions *[]client.Version) decoder {
	return decoder{}
}

func (this *decoder) Decode(buf []byte) error {
	err := xml.Unmarshal(buf, this)
	if err != nil {
		return err
	}
	return nil
}

func (this *decoder) Encode() (error, []byte) {
	buf, err := xml.MarshalIndent(this, "", "	")
	if err != nil {
		return err, nil
	}
	return nil, buf
}
