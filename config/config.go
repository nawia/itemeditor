package config

import (
	"gitlab.com/nawia/itemeditor/config/item"
)

type Config struct {
	Item item.Config
}

func New(args []string) (error, Config) {
	ret := Config{}
	var err error
	err, ret.Item = item.New(args)
	return err, ret
}
