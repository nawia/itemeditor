package item

import (
	"fmt"
	"path"
	"strings"
)

type Config struct {
	Otb  *string
	Json *string
	Xml  *string
}

func New(args []string) (error, Config) {
	ret := Config{}

	if len(args) < 2 {
		err := fmt.Errorf("Usage: %s /path/to/file.json\n", args)
		return err, ret
	}

	filePath := args[1]

	if strings.HasSuffix(filePath, ".json") {
		ret.Json = &filePath
	} else if strings.HasSuffix(filePath, ".otb") {
		ret.Otb = &filePath
	} else if strings.HasSuffix(filePath, ".xml") {
		ret.Xml = &filePath
	} else {
		otbPath := path.Join(filePath, "items.otb")
		xmlPath := path.Join(filePath, "items.xml")
		ret.Otb = &otbPath
		ret.Xml = &xmlPath
	}

	return nil, ret
}
