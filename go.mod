module gitlab.com/nawia/itemeditor

go 1.16

require (
	github.com/go-playground/validator/v10 v10.10.1
	github.com/julienschmidt/httprouter v1.3.0
)
