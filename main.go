package main

import (
	"fmt"
	"os"

	"gitlab.com/nawia/itemeditor/config"
	"gitlab.com/nawia/itemeditor/http"
	jsonItem "gitlab.com/nawia/itemeditor/json/item"
	"gitlab.com/nawia/itemeditor/model/client"
	"gitlab.com/nawia/itemeditor/model/item"
	otbItem "gitlab.com/nawia/itemeditor/otb/item"
	xmlItem "gitlab.com/nawia/itemeditor/xml/item"

	validator "github.com/go-playground/validator/v10"
)

func main() {
	var err error

	err, config := config.New(os.Args)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	items := item.List{}
	itemVersions := []item.Version{}
	clientVersions := []client.Version{}

	if config.Item.Json != nil {
		buf, err := os.ReadFile(*config.Item.Json)
		if err != nil {
			fmt.Println("Failed to read the file: ", err)
			return
		}

		decoder := jsonItem.Decoder(&items)
		err = decoder.Decode(buf)
		if err != nil {
			fmt.Println("Failed to decode the json", err)
			return
		}
	}

	if config.Item.Otb != nil {
		buf, err := os.ReadFile(*config.Item.Otb)
		if err != nil {
			fmt.Println("Failed to read the file: ", err)
			return
		}

		decoder := otbItem.Decoder(&items, &itemVersions, &clientVersions)
		err = decoder.Decode(buf)
		if err != nil {
			fmt.Println("Failed to decode the json", err)
			return
		}
	}

	if config.Item.Xml != nil {
		buf, err := os.ReadFile(*config.Item.Xml)
		if err != nil {
			fmt.Println("Failed to read the file: ", err)
			return
		}

		decoder := xmlItem.Decoder(&items, &itemVersions, &clientVersions)
		err = decoder.Decode(buf)
		if err != nil {
			fmt.Println("Failed to decode the json", err)
			return
		}
		err, buf = decoder.Encode()
		if err != nil {
			fmt.Println("Failed to decode the json", err)
			return
		}
		fmt.Println(string(buf))
	}

	validate := validator.New()
	for itemId := range items {
		err = validate.Struct(items[itemId])
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	h := http.New()
	go func() {
		for {
			select {
			case req := <-h.OnListItems:
				itemListCopy := items

				if req.Flags != nil {
					itemListCopy = itemListCopy.FilterFlagsAny(*req.Flags)
				}

				if req.Group != nil {
					itemListCopy = itemListCopy.FilterGroupEq(*req.Group)
				}

				if req.Name != nil {
					itemListCopy = itemListCopy.FilterNameContains(*req.Name)
				}

				if req.Speed != nil {
					itemListCopy = itemListCopy.FilterSpeedGte(*req.Speed)
				}

				req.List <- []item.Item(items)
			}
		}
	}()
	err = h.Listen()
	if err != nil {
		fmt.Println(err)
		return
	}

	return
}
