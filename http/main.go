package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/nawia/itemeditor/model/item"
)

func New() Server {
	ret := Server{}
	ret.r = httprouter.New()
	ret.OnListItems = make(chan OnListItems)
	ret.r.GET("/items", ret.List)
	return ret
}

func (this *Server) Listen() error {
	return http.ListenAndServe(":8080", this.r)
}

func (this *Server) List(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	msg := OnListItems{}
	msg.List = make(chan []item.Item)

	if flags, exists := r.URL.Query()["flags"]; exists {
		if len(flags) == 0 {
			w.WriteHeader(400)
			w.Write([]byte("When specifying flags parameter, at least 1 flag is required"))
			return
		}

		for flagId := range flags {
			valid := false
			for validFlagId := range item.Flags {
				if flags[flagId] == item.Flags[validFlagId] {
					valid = true
				}
			}
			if !valid {
				w.WriteHeader(400)
				w.Write(
					[]byte(
						fmt.Sprintf(
							"Invalid flag '%s' specified. Allowed flags are: AllowDistRead AlwaysOnTop Animation BlockPathFind BlockProjectile BlockSolid CannotDecay ClientCharges FloorChangeDown FloorChangeEast FloorChangeNorth FloorChangeSouth FloorChangeWest Hangable HasHeight Horizontal LookThrough Movable Pickupable Readable Rotable Stackable Unused Usable Vertical WalkStack",
							flags[flagId],
						),
					),
				)
				return
			}
		}

		msg.Flags = &flags
	}

	if group, exists := r.URL.Query()["group"]; exists {
		if len(group) != 1 {
			w.WriteHeader(400)
			w.Write([]byte("Exactly 1 group parameter is required"))
			return
		}

		msg.Group = &group[0]
	}

	if name, exists := r.URL.Query()["name"]; exists {
		if len(name) != 1 {
			w.WriteHeader(400)
			w.Write([]byte("Exactly 1 name parameter is required"))
			return
		}

		msg.Name = &name[0]
	}

	if speed, exists := r.URL.Query()["speed"]; exists {
		n, err := strconv.ParseUint(speed[0], 10, 16)
		if err != nil {
			w.WriteHeader(400)
			w.Write([]byte("Speed needs to be a valid positive integer at max of 65535"))
			return
		}

		val := uint16(n)
		msg.Speed = &val
	}

	this.OnListItems <- msg
	list := <-msg.List
	buf, err := json.Marshal(list)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(200)
	w.Write(buf)
}

type OnListItems struct {
	Flags *[]string
	Group *string
	Name  *string
	Speed *uint16
	List  chan []item.Item
}

type Server struct {
	r           *httprouter.Router
	OnListItems chan OnListItems
}
