package item

import (
	"encoding/json"

	"gitlab.com/nawia/itemeditor/model/item"
)

type decoder struct {
	*item.List
}

func Decoder(list *item.List) decoder {
	return decoder{list}
}

func (this *decoder) Decode(buf []byte) error {
	data := Definitions{}
	err := json.Unmarshal(buf, &data)
	if err != nil {
		return err
	}
	for i := range data.Items {
		*this.List = append(*this.List, data.Items[i])
	}
	return nil
}

type Definitions struct {
	Items []item.Item `json:"items"`
}
