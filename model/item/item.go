package item

type Item struct {
	ClientId     uint16          `json:"clientId"`
	Flags        map[string]bool `json:"flags" validate:"dive,keys,oneof=AllowDistRead AlwaysOnTop Animation BlockPathFind BlockProjectile BlockSolid CannotDecay ClientCharges FloorChangeDown FloorChangeEast FloorChangeNorth FloorChangeSouth FloorChangeWest Hangable HasHeight Horizontal LookThrough Movable Pickupable Readable Rotable Stackable Unused Usable Vertical WalkStack,endkeys"`
	Group        string          `json:"group" validate:"oneof=NONE GROUND CONTAINER WEAPON AMMUNITION ARMOR CHARGES ITEM_GROUP_TELEPORT ITEM_GROUP_MAGICFIELD ITEM_GROUP_WRITEABLE ITEM_GROUP_KEY ITEM_GROUP_SPLASH ITEM_GROUP_FLUID ITEM_GROUP_DOOR ITEM_GROUP_DEPRECATED ITEM_GROUP_LAST"`
	Hash         [16]byte        `json:"hash"`
	Light2       Light           `json:"light2"`
	MiniMapColor uint16          `json:"miniMapColor"`
	Name         string          `json:"name"`
	ServerId     uint16          `json:"serverId"`
	Speed        uint16          `json:"speed"`
	TopOrder     uint8           `json:"topOrder"`
	WareId       uint16          `json:"wareId"`
}

var Flags = []string{
	"AllowDistRead",
	"AlwaysOnTop",
	"Animation",
	"BlockPathFind",
	"BlockProjectile",
	"BlockSolid",
	"CannotDecay",
	"ClientCharges",
	"FloorChangeDown",
	"FloorChangeEast",
	"FloorChangeNorth",
	"FloorChangeSouth",
	"FloorChangeWest",
	"Hangable",
	"HasHeight",
	"Horizontal",
	"LookThrough",
	"Movable",
	"Pickupable",
	"Readable",
	"Rotable",
	"Stackable",
	"Unused",
	"Usable",
	"Vertical",
	"WalkStack",
}

type Light struct {
	Color uint16 `json:"color"`
	Level uint16 `json:"level"`
}

type Version uint32
