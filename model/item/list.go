package item

import "strings"

type List []Item

func NewList(items []Item) List {
	return List(items)
}

func (this List) FilterFlagsAny(flags []string) List {
	ret := List{}
	for itemId := range this {
		for flagId := range flags {
			if this[itemId].Flags[flags[flagId]] == true {
				ret = append(ret, this[itemId])
			}
		}
	}
	return ret
}

func (this List) FilterGroupEq(group string) List {
	ret := List{}
	for itemId := range this {
		if this[itemId].Group == group {
			ret = append(ret, this[itemId])
		}
	}
	return ret
}

func (this List) FilterNameContains(name string) List {
	ret := List{}
	for itemId := range this {
		if strings.Contains(this[itemId].Name, name) {
			ret = append(ret, this[itemId])
		}
	}
	return ret
}

func (this List) FilterSpeedGte(speed uint16) List {
	ret := List{}
	for itemId := range this {
		if this[itemId].Speed >= speed {
			ret = append(ret, this[itemId])
		}
	}
	return ret
}
