package item

import (
	"gitlab.com/nawia/itemeditor/model/client"
	"gitlab.com/nawia/itemeditor/model/item"
)

/*
#include "otb.h"
*/
import "C"

func VersionInfo(cVersion C.VersionInfo) (item.Version, client.Version) {
	i := item.Version(cVersion.otb_version)
	c := client.Version{uint32(cVersion.client_version), uint32(cVersion.build)}
	return i, c
}
