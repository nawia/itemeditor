package item

/*
#include "otb.h"
*/
import "C"

var itemFlags = map[C.ItemFlags]string{
	C.ITEM_FLAG_BLOCK_SOLID:      "BlockSolid",
	C.ITEM_FLAG_BLOCK_PROJECTILE: "BlockProjectile",
	C.ITEM_FLAG_BLOCK_PATHFIND:   "BlockPathFind",
	C.ITEM_FLAG_HAS_HEIGHT:       "HasHeight",
	C.ITEM_FLAG_USABLE:           "Usable",
	C.ITEM_FLAG_PICKUPABLE:       "Pickupable",
	C.ITEM_FLAG_MOVABLE:          "Movable",
	C.ITEM_FLAG_STACKABLE:        "Stackable",
	C.ITEM_FLAG_FLOORCHANGEDOWN:  "FloorChangeDown",
	C.ITEM_FLAG_FLOORCHANGENORTH: "FloorChangeNorth",
	C.ITEM_FLAG_FLOORCHANGEEAST:  "FloorChangeEast",
	C.ITEM_FLAG_FLOORCHANGESOUTH: "FloorChangeSouth",
	C.ITEM_FLAG_FLOORCHANGEWEST:  "FloorChangeWest",
	C.ITEM_FLAG_ALWAYSONTOP:      "AlwaysOnTop",
	C.ITEM_FLAG_READABLE:         "Readable",
	C.ITEM_FLAG_ROTABLE:          "Rotable",
	C.ITEM_FLAG_HANGABLE:         "Hangable",
	C.ITEM_FLAG_VERTICAL:         "Vertical",
	C.ITEM_FLAG_HORIZONTAL:       "Horizontal",
	C.ITEM_FLAG_CANNOTDECAY:      "CannotDecay",
	C.ITEM_FLAG_ALLOWDISTREAD:    "AllowDistRead",
	C.ITEM_FLAG_UNUSED:           "Unused",
	C.ITEM_FLAG_CLIENTCHARGES:    "ClientCharges", //deprecated
	C.ITEM_FLAG_LOOKTHROUGH:      "LookThrough",
	C.ITEM_FLAG_ANIMATION:        "Animation",
	C.ITEM_FLAG_WALKSTACK:        "WalkStack",
}

func Flags(cFlags C.ItemFlags) map[string]bool {
	ret := map[string]bool{}
	for k, v := range itemFlags {
		ret[v] = cFlags&k == k
	}
	return ret
}
