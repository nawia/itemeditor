package item

import (
	"gitlab.com/nawia/itemeditor/model/client"
	"gitlab.com/nawia/itemeditor/model/item"
)

/*
#include "otb.h"
extern void onItemTypeWrapper(ItemType item);
extern void onOTBVersionWrapper(VersionInfo version);
*/
import "C"

type decoder struct {
	ItemVersions   *[]item.Version
	ClientVersions *[]client.Version
	*item.List
}

func Decoder(list *item.List, itemVersion *[]item.Version, clientVersion *[]client.Version) decoder {
	ret := decoder{itemVersion, clientVersion, list}
	_decoder = append(_decoder, ret)
	return ret
}

func (this decoder) Decode(buf []byte) error {
	parser := C.ParserOTB{}
	parser.onItemType = C.OnItemType(C.onItemTypeWrapper)
	parser.onOTBVersion = C.OnOTBVersion(C.onOTBVersionWrapper)
	C.parseOTB(parser, C.ulong(len(buf)), C.CBytes(buf))
	return nil
}

// Global for C Wrappers
var _decoder = []decoder{}

//export onItemTypeGo
func onItemTypeGo(cItem C.ItemType) {
	item := Item(cItem)

	for i := range _decoder {
		*_decoder[i].List = append(*_decoder[i].List, item)
	}
}

//export onOTBVersionGo
func onOTBVersionGo(cVersion C.VersionInfo) {
	itemVersion, clientVersion := VersionInfo(cVersion)
	for i := range _decoder {
		*_decoder[i].ItemVersions = append(*_decoder[i].ItemVersions, itemVersion)
		*_decoder[i].ClientVersions = append(*_decoder[i].ClientVersions, clientVersion)
	}
}
