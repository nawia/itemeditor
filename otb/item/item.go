package item

import (
	"gitlab.com/nawia/itemeditor/model/item"
)

/*
#include "otb.h"
*/
import "C"

func Hash(hash [16]C.uchar) [16]byte {
	ret := [16]byte{}
	for i := range hash {
		ret[i] = byte(hash[i])
	}
	return ret
}

func Item(cItem C.ItemType) item.Item {
	ret := item.Item{}
	ret.Name = C.GoStringN(cItem.name.ptr, C.int(cItem.name.length))
	ret.ClientId = uint16(cItem.clientId)
	ret.Flags = Flags(cItem.flags)
	ret.Group = Group(cItem.group)
	ret.Hash = Hash(cItem.hash)
	ret.Light2 = Light(cItem.light2)
	ret.MiniMapColor = uint16(cItem.miniMapColor)
	ret.ServerId = uint16(cItem.serverId)
	ret.Speed = uint16(cItem.speed)
	ret.TopOrder = uint8(cItem.topOrder)
	ret.WareId = uint16(cItem.wareId)
	return ret
}

func Light(cLight C.Light) (ret item.Light) {
	ret.Color = uint16(cLight.color)
	ret.Level = uint16(cLight.level)
	return
}
