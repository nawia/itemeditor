const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const stage = process.env.NODE_ENV || "development";

module.exports = {
	entry: './js/index.js',
	mode: stage,
	module: {
		rules: [
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.pug$/,
				use: {
					loader: 'pug-loader'
				}
			}
		],
		// alias: {
		// 	'Config': path.resolve('./js/
		// }
	},
	plugins: [
		new webpack.DefinePlugin({
			config: JSON.stringify(
				{
					stage: stage,
					itemsUri: 'http://localhost:8080/items'
				}
			)
		}),
		new HtmlWebpackPlugin({
			template: 'pug/index.pug',
			templateParameters: {
				title: 'Item Editor'
			}
		})
	]
};
