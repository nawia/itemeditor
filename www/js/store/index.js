import { type, mapObjIndexed } from 'ramda';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import  logger from 'redux-logger';



const combineReducersRec = v =>
	type(v) === 'Object' ?
		combineReducers(mapObjIndexed(combineReducersRec,v)) :
		v

export const create = reducers => createStore(
	combineReducersRec(reducers),
	{},
	applyMiddleware(logger)
);
