// import { Observable } from 'rxjs';
import { create } from './store/index.js';
import Reducer from './reducer/index.js';
import { open } from './action/page.js';

const store = create(Reducer);

store.subscribe(()=>console.log(store.getState()));

store.dispatch(open());
