import { propEq, nthArg, tap, ifElse, identity, compose, assocPath } from 'ramda';
import { OPEN, CLOSE, TOGGLE } from '../action/page.js';

export function opened(state = false, action) {
	switch( action.type )
	{
		case OPEN:
			return state = true;
		case CLOSE:
			return state = false;
		case TOGGLE:
			return state = !state;
		default:
			return state;
	}
};

export function closed(state = true, action) {
	switch( action.type )
	{
		case OPEN:
			return state = false;
		case CLOSE:
			return state = true;
		case TOGGLE:
			return state = !state;
		default:
			return state;
	}
}
export default { opened, closed };
