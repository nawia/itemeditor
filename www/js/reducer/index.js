import { reduce, map, identity, compose, concat } from 'ramda';
import page from './page.js';

export { page };

export default { page };
