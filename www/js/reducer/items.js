function items(state = [], action) {
  switch (action.type) {
    case action.FETCHED_FROM_SERVER:
      return state.concat([action.text])
    default:
      return state
  }
}
