import { nthArg, applySpec, always } from 'ramda';

const ITEM_FETCHED_FROM_SERVER = 'ITEM_FETCHED_FROM_SERVER';

const fetchedFromServer = applySpec({
	'type': always(ITEM_FETCHED_FROM_SERVER),
	'item': nthArg(0)
});
