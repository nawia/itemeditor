import { always } from 'ramda';

export const OPEN = 'PAGE_OPEN';

export const CLOSE = 'PAGE_CLOSE';

export const TOGGLE = 'PAGE_TOGGLE';

export const open = always(({'type': OPEN}));

export const close = always(({'type': CLOSE}));

export const toggle = always(({'type': TOGGLE}));
