import { always, applySpec } from 'ramda';

export const OPEN = 'window-open';

export const open = applySpec({
	'type': always(OPEN)
});
